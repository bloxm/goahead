
##############################################
# OpenWrt Makefile for goahead program
#
#
# Most of the variables used here are defined in
# the include directives below. We just need to 
# specify a basic description of the package, 
# where to build our program, where to find 
# the source files, and where to install the 
# compiled program on the router. 
# 
# Be very careful of spacing in this file.
# Indents should be tabs, not spaces, and 
# there should be no trailing whitespace in
# lines that are not commented.
# 
##############################################

include $(TOPDIR)/rules.mk

# Name and release number of this package
PKG_NAME:=goahead
PKG_RELEASE:=1

GOAHEAD_MAKE_OPTS:=SHOW=1
GOAHEAD_MAKE_OPTS+=ARCH=mips
GOAHEAD_MAKE_OPTS+=DEBUG=release
GOAHEAD_MAKE_OPTS+=PROFILE=static
GOAHEAD_MAKE_OPTS+=ME_DEBUG=0
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_AUTHHEADER=\'\"BLX-Authenticate\"\'
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_LISTEN=\'\"http://127.0.0.1:80,https://127.0.0.1:443\"\'
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_STATIC=1
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_CGI=1
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_SSL=1
GOAHEAD_MAKE_OPTS+=ME_COM_SSL=1
GOAHEAD_MAKE_OPTS+=ME_COM_MBEDTLS=1
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_ACCESS_LOG=0
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_DIGEST=1
#GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_REPLACE_MALLOC=1
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_REALM=\'\"private\"\'
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_CGI_BIN=\'\"/www/adminportal\"\'
GOAHEAD_MAKE_OPTS+=CC="$(TARGET_CC)"
GOAHEAD_MAKE_OPTS+=LD="$(TARGET_LD)"
GOAHEAD_MAKE_OPTS+=CFLAGS="$(TARGET_CFLAGS) -fPIC"
GOAHEAD_MAKE_OPTS+=LDFLAGS="$(TARGET_LDFLAGS) --static"
GOAHEAD_MAKE_OPTS+=CROSS="$(TARGET_CROSS)"
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_DISABLE_CACHE=1
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_SSL_CERTIFICATE=\'\"/etc/ssl/self.crt\"\'
GOAHEAD_MAKE_OPTS+=ME_GOAHEAD_SSL_KEY=\'\"/etc/ssl/self.key\"\'

#goahead --debugger --route /etc/route.txt --auth /etc/pass.txt --log /tmp/golog:9
#sample route file
#route uri=/blxadmin/cgi-bin/private/ dir=/www/adminportal/cgi-bin/private auth=digest handler=cgi
#route uri=/cgi-bin/private/ dir=/www/adminportal/cgi-bin/private auth=digest handler=cgi
#route uri=/blxadmin/cgi-bin/ dir=/www/adminportal/cgi-bin handler=cgi
#route uri=/cgi-bin/ dir=/www/adminportal/cgi-bin handler=cgi
#route uri=/blxadmin/ dir=/www/adminportal
#route uri=/ dir=/www/adminportal




# This specifies the directory where we're going to build the program.  
# The root build directory, $(BUILD_DIR), is by default the build_mipsel 
# directory in your OpenWrt SDK directory
PKG_BUILD_DIR := $(BUILD_DIR)/$(PKG_NAME)


include $(INCLUDE_DIR)/package.mk



# Specify package information for this program. 
# The variables defined here should be self explanatory.
# If you are running Kamikaze, delete the DESCRIPTION 
# variable below and uncomment the Kamikaze define
# directive for the description below
define Package/goahead
	SECTION:=utils
	CATEGORY:=Utilities
	TITLE:=goahead
	DEPENDS:=+libpthread
endef

#To add a dependency, modify DEPENDS, above
#This adds a dependency to our custom library - +libspcdns+codec



# Uncomment portion below for Kamikaze and delete DESCRIPTION variable above
define Package/goahead/description
endef


# Specify what needs to be done to prepare for building the package.
# In our case, we need to copy the source files to the build directory.
# This is NOT the default.  The default uses the PKG_SOURCE_URL and the
# PKG_SOURCE which is not defined here to download the source from the web.
# In order to just build a simple program that we have just written, it is
# much easier to do it this way.
define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ../goahead-upstream/* $(PKG_BUILD_DIR)/
endef


# We do not need to define Build/Configure or Build/Compile directives
# The defaults are appropriate for compiling a simple program such as this one
define Build/Compile
	$(MAKE) -C $(PKG_BUILD_DIR) $(GOAHEAD_MAKE_OPTS)
endef

# Specify where and how to install the program. Since we only have one file, 
# the dnsrewrite executable, install it by copying it to the /bin directory on
# the router. The $(1) variable represents the root directory on the router running 
# OpenWrt. The $(INSTALL_DIR) variable contains a command to prepare the install 
# directory if it does not already exist.  Likewise $(INSTALL_BIN) contains the 
# command to copy the binary file from its current location (in our case the build
# directory) to the install directory.
define Package/goahead/install
	$(INSTALL_DIR) $(1)/bin
	$(INSTALL_DIR) $(1)/etc/ssl
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/build/linux-mips-static/bin/goahead $(1)/bin/
	cp ssl/self.* $(1)/etc/ssl
endef

define Package/goahead/postinst

endef

define Package/goahead/clean
	$(MAKE) -C $(PKG_BUILD_DIR) clean
endef

# This line executes the necessary commands to compile our program.
# The above define directives specify all the information needed, but this
# line calls BuildPackage which in turn actually uses this information to
# build a package.
$(eval $(call BuildPackage,goahead))
